import couchdb
import setting

couch = couchdb.Server("https://%s.cloudant.com" % setting.CREDIT['username'])
couch.resource.credentials = (setting.CREDIT['username'],
                              setting.CREDIT['password'])

# or, creating one
db = couch.create('test')
# create a doc and save it.
doc_id, doc_rev = db.save({
      'name': 'Mike Broberg',
      'title': 'Fun Captain',
      'superpower': 'More fun than a hallucinogenic trampoline'
})

# Query results are treated as iterators, like this:

# print all docs in the database

for doc in db:
    print doc
# or, do the same on a pre-defined index
# where 'DDOC/INDEX' maps to '_design/DDOC/_view/INDEX'
# in the HTTP API


if __name__ == '__main__':
    pass
