#! /usr/bin/env python
# -*- coding: utf-8 -*-
#
# Author: Victor Huang
# Date: 2015.11.24
#
# License: BSD 3 clause
#
# Name: getPTTBoard.py
# Description: get the post content and push of a PTT board
# Arguments: ./getPTTBoard.py board_name index_page_num1 index_page_num2 dir_name
#	     it will get pages from index index_page_num1 to index_page_num2#

# import pycurl
import sys
from bs4 import BeautifulSoup
import logging
import urllib2
import argparse

import json
logger = logging.getLogger( __name__  )
logging.basicConfig(filename="log", level=logging.DEBUG)

def trash_detactor(output):
    pass

class GetPage:
    def __init__ (self, url):
        self.contents = ''
        self.url = url

    def read_page (self, buf):
        self.contents = self.contents + buf

    def get_page (self):
        return self.contents

def Download(href):
	page = GetPage(href)
	response = urllib2.urlopen(page.url)
	page.read_page(response.read())
	return page

def weekday_format(wd_str):
	wd_dict = {
                'Mon' :'1' ,
                'Tue' :'2' ,
                'Wed' :'3' ,
                'Thu' :'4' ,
                'Fri' :'5' ,
                'Sat' :'6' ,
                'Sun' :'7' ,
	}
	if wd_str.strip() in wd_dict:
		return wd_dict[wd_str.strip()]

def month_format(month_str):
	mon_dict = {
                'Jan' :'1' ,
                'Feb' :'2' ,
                'Mar' :'3' ,
                'Apr' :'4' ,
                'May' :'5' ,
                'Jun' :'6' ,
                'Jul' :'7' ,
                'Aug' :'8' ,
                'Sep' :'9' ,
                'Oct' :'10',
                'Nov' :'11',
                'Dec' :'12',
	}
	if month_str.strip() in mon_dict:
		return mon_dict[month_str.strip()]

def time_format(time_string):
	time_es = time_string.strip().split()
	if len(time_es) == 5 and len(time_es[-1]) == 4:
		weekday = weekday_format(time_es[0])
		month= month_format(time_es[1])
		day = time_es[2]
		time = time_es[3]
		year = time_es[4]

		date = "/".join([year, month, day])
		date = ":".join([date, weekday])
		datetime = ' '.join([date, time])

		return datetime
	else:
		print 'time format illegal'


def main():
    ap = argpar.ArgumentParser()
    ap.add_argument("board_name", type=str, help="Board to download posts")
    ap.add_argument("start_page", type=str, help="start page number")
    ap.add_argument("end_page", type=str, help="end page number ")
    ap.add_argument("out_dir", type=str,
                    help="the directory to store the ptt post result, \
                    note the pushes will be save to the directory \
                    <out_dir>_push, you shall create it ahead.")

    args = ap.parse_args()
    '''
	board_name = sys.argv[1]
	board = "https://www.ptt.cc/bbs/{0}/".format(sys.argv[1])
	start = int(sys.argv[2])
	end = int(sys.argv[3])
	out_dir = sys.argv[4]
    '''

    board_name = args.board_name
    board   = "https://www.ptt.cc/bbs/{0}/".format(board_name)
    start   = int(args.start_page)
    end     = int(args.end_page)
    out_dir = args.out_dir

    for num in range(start,end+1):
		print "{0}: Get page {1}...".format(board_name,num)
		# get links page
		href = board+"index{0}.html".format(num)
		page = Download(href)
		links = []
		soup = BeautifulSoup(page.get_page())
		# extract links
		s = soup.find_all(class_="r-ent")
		for e in s:
			a = e.find(class_="title").a
			if a != None:
				links.append(a.get('href'))
		# get page of each link
		for index, link in enumerate(links):
			print 'parsing post no.', index
			href = str("https://www.ptt.cc"+link)
			try:
				page = Download(href)
				soup = BeautifulSoup(page.get_page())
				main_content_div= soup.find(id="main-content")
				# content exists in the place in the main-content
				user     = main_content_div.div.span.next_sibling.get_text()
				title 	 = main_content_div.div.next_sibling.next_sibling.span.next_sibling.get_text()
				time_str = main_content_div.div.next_sibling.next_sibling.next_sibling.span.next_sibling.get_text()
				datetime = time_format(time_str)

                                content = u""
                                content_div = main_content_div.div
                                content_a = main_content_div.a
                                while content_div is not None or content_a is not None:
                                    if content_div is not None:
                                        c_text = content_div.string
                                        if c_text is not None:
                                            c_text = c_text.strip()
                                            if c_text.find(u'※ 發信站') == -1 and c_text.find(u'※ 文章網址') == -1 and c_text.find(u'※ 編輯') == -1:
                                                content += c_text.strip() + u'\n'
                                        content_div = content_div.next_sibling

                                    if content_a is not None:
                                        c_a = content_a.string
                                        if c_a is not None:
                                            content += c_a.strip() + u'\n'
                                        content_a = content_a.next_sibling
				#content  = main_content_div.div.next_sibling.next_sibling.next_sibling.next_sibling

				pushes = []
				push_divs = soup.find_all("div", {'class': 'push'})
				for push_div in push_divs:
					push_type = push_div.span.get_text().strip()
					if push_type == u'推':
						push_type = u'1'
					elif push_type == u'噓':
						push_type = u'-1'
					else:
						push_type = u'0'

					push_user = push_div.span.next_sibling.get_text().strip()
					push_content = push_div.span.next_sibling.next_sibling.get_text().strip()
					if len(push_content) > 3:
						push_content = push_content[2:]
					push_time = push_div.span.next_sibling.next_sibling.next_sibling.get_text().strip()
					if push_type is not None and push_user is not None and push_time is not None:
						pushes.append((push_type, push_user,push_time,push_content))

			except:
				logger.error(href)
				continue
			if len(content) != 0:
				with open(out_dir+'/'+str(num)+'-'+str(index)+'.txt','w') as f:
					if user is not None:
						f.write((user + '\n').encode('utf-8'))
					if title is not None:
						f.write((title + '\n').encode('utf-8'))
					if datetime is not None:
						f.write((datetime + '\n').encode('utf-8'))
					if datetime is not None or title is not None:
						f.write('--\n'.encode('utf-8'))
					f.write(content.encode('utf-8'))


				with open(out_dir[:-1]+'_push/'+str(num)+'-'+str(index)+'.txt','w') as push_f:
					for p in pushes:
						push_f.write((u','.join(p) + u'\n').encode('utf-8'))

def data_to_json(user, title, datetime, pushes):
    data = {'user': user,
                        'title': titile,
                        'datetime': datetime,
                        'push': [(u','.join(p) + u'\n').encode('utf-8')
                                 for p in pushes]
                        }

if __name__ == '__main__':
    main()

