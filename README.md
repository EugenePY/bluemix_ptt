
# My Second Bluemix Program.

##### *This module trying to deploy a web crawler to Bluemix.

The crawler is written in Python. So the runtime Python(or Pypy) ver 2.7 is required.

There are two ways to do this.
1. Writing a dockerfile(Docker). 
1. Writing a Python buildpack.(Cloud Foundary) add Conda instead pip.
3. Openstack.

We demo above approach to deploy a simple PTT web crawler on IBM Bluemix.

## OS

Ubunbu 14.0.1

#### Dependency

1. Beautiful Soup
2. PyMongo(it seems )
3. Scrapy


Bluemix is using Cloud Foundary, Docker conatainer, and Openstack to deploy
your local application to the Bluemix Cloud.

Since Cloud Foundary providing a very usful command line tool, we can easily 
deploy the application with your terminal command line.

#### Pre-requirement
Please install the following tool to finish the tutorial.

* [Cloud Foundary command line interface](https://docs.cloudfoundry.org/cf-cli/install-go-cli.html)
* [IBM Bluemix container Cloud Foundary command line plug-in](https://console.ng.bluemix.net/docs/containers/container_cli_cfic.html) 
* [Docker](https://docs.docker.com/mac)

#### Cloud Technology
**TODO**


#### Tutorial

###### Deploy a hello word program via Python Conda Buildpack 

In here we use a the sample git repository from IBM.

* [python hello word flask](https://github.com/IBM-Bluemix/python-hello-world-flask.git)

For cf commnad line the default OS is 
clone the repo first.

> git clone http://github.com/IBM-Bluemix/python-hello-world-flask.git 

The basic idea of deploying the app on Bluemix is using the buildpack to setup 
your runtime enviorment and run the application. 

The structure of the repo is very simple. The repo contains {hello.py, 
Profile, manifest.yml, requirements.txt}

What these file do is, 
	profile is inital the app
	manifest.yml is describing the container's information which containing the 
	application.
	requirements.txt is containing the application dependency.

we can simily deploy the application by following commnad.
> cf login [-a API_URL] [-u USERNAME] [-p PASSWORD] [-o ORG] [-s SPACE]
> cf push python-hello-word-flask

###### Deploy connect to a service with python application

We now demo how to create a Cloudant Non-SQL service bind to a simlple python program.
The program creates a data and sent the data to the Database through http request. 

###### A simple buildpack
compile file
**TODO**

###### create a testing env..
since it is quite slow for debuging when you need to recompile the application. 

###### Deploy a docker image 

We pull a Hadoop docker image and push to the Bluemix.
**TODO**

###### Deploy a docker cluster via Docker Swarm
**TODO**

###### Example: Distributed Tensorflow with Bluemix
**TODO**

###### Hybrid Cloud GPU cluster
**TODO**

###### Bluemix From Platform as a Service to Infrastructure as a Service.
**TODO**

